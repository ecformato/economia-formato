//Constantes y variables relativas a la Home
const commonElements = 'https://api.elconfidencial.com/service/resource/dashboard-economia-elementos-comunes/';
const dataProduccion = 'https://api.elconfidencial.com/service/resource/dashboard-economia-produccion/';
const dataExpectativas = 'https://api.elconfidencial.com/service/resource/dashboard-economia-expectativas/';
const dataCuentasPublicas = 'https://api.elconfidencial.com/service/resource/dashboard-economia-cuentas-publicas/';
const dataSectores = 'https://api.elconfidencial.com/service/resource/dashboard-economia-sectores/';
const dataEmpleo = 'https://api.elconfidencial.com/service/resource/dashboard-economia-empleo/';
const dataDemanda = 'https://api.elconfidencial.com/service/resource/dashboard-economia-demanda/';

const commonElementsLocal = './local-data/elementos-comunes.tsv';
const dataProduccionLocal = './local-data/produccion.tsv';
const dataCuentasPublicasLocal = './local-data/cuentas-publicas.tsv';
const dataExpectativasLocal = './local-data/expectativas.tsv';
const dataSectoresLocal = './local-data/sectores.tsv';
const dataEmpleoLocal = './local-data/empleo.tsv';
const dataDemandaLocal = './local-data/demanda.tsv';

//Variables de almacenamiento de datos >> Borrar los que no sean necesarios
let commonElementsMap = new Map();
let produccionPib = [], produccionPibEurozona = [], produccionInversionProductiva = [], produccionEmpresasActivas = [], produccionProdIndustrial = [], produccionPibEmpleo = [];
let expectativasPrevisiones = [], expectativasPMI = [], expectativasConfianzaConsumidor = [], expectativasHogaresEconomia = [], expectativasHogaresParo = [];
let cuentasDeficitHistorico = [], cuentasRecaudacion = [], cuentasDeudaPublica = [], cuentasDeficitCCAA = [];
let sectoresCambioAfiliacion = [], sectoresValorAnadido = [], sectoresLlegadaViajeros = [];
let empleoAfiliacionProvincias = [], empleoAfiliacionHistorico = [], empleoTasaParoHistorica = [], empleoCaidaEmpleo = [], empleoTasaParoEuropa = [], empleoNumeroParados = [];
let demandaMinoristaHistorica = [], demandaMinoristaSectorial = [], demandaMinoristaCCAA = [], demandaMinoristaEurozona = [];

//Variables Helpers
let tooltipProduccion = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__produccion');

let tooltipExpectativas = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__expectativas');

let tooltipCuentas = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__cuentas');

let tooltipEmpleo = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__empleo');

let tooltipDemanda = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__demanda');

let tooltipSectores = d3.select('body')
    .append('div')
    .attr('class', 'tooltip-dashboard tooltip-dashboard__sectores');

let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
let mesesReducido = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

//Carga y visualización de datos
let env = 'pro';
if(env == 'local'){
    getData(commonElementsLocal, 'commonElements');
    getData(dataProduccionLocal, 'dataProduccion');
    getData(dataExpectativasLocal, 'dataExpectativas');
    getData(dataCuentasPublicasLocal, 'dataCuentasPublicas');
    getData(dataSectoresLocal, 'dataSectores');
    getData(dataEmpleoLocal, 'dataEmpleo');
    getData(dataDemandaLocal, 'dataDemanda');
    createCharts();
    leaveCharts();
} else {
    getData(commonElements, 'commonElements');
    getData(dataProduccion, 'dataProduccion');
    getData(dataExpectativas, 'dataExpectativas');
    getData(dataCuentasPublicas, 'dataCuentasPublicas');
    getData(dataSectores, 'dataSectores');
    getData(dataEmpleo, 'dataEmpleo');
    getData(dataDemanda, 'dataDemanda');
    createCharts();
    leaveCharts();
}

/* Visualización de gráficos */
function createCharts() {
    getProduccionPibHistograma(produccionPib);
    getDemandaHistorica(demandaMinoristaHistorica);
    getExpectativasConfianzaConsumidor(expectativasConfianzaConsumidor);
    getExpectativasPrevisionParo(expectativasHogaresParo)
    getEmpleoAfiliacionHistorico(empleoAfiliacionHistorico)
    getProduccionEmpresasActivas(produccionEmpresasActivas);
    getCuentasDeudaPublica(cuentasDeudaPublica);
    getCuentasRecaudacion(cuentasRecaudacion);
    getExpectativasPrevisiones(expectativasPrevisiones);
}

/*
*
* Funciones específicas para cada gráfico
*
*/
function getProduccionPibHistograma(data){
    let grafico = document.getElementById('produccion-pib-trimestral');
    let lineaEjeX = 'crecimiento-trim-ejeX';
    
    //Estructura fundamental del gráfico
    let margin = {top: 10, right: 10, bottom: 20, left: 25},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top +')');

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleTime()
        .range([0,width])
        .domain(d3.extent(data, function(d) { return formatTime(d.trimestre_v2) }));

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x2).ticks(8).tickFormat(function(d){ return d.getFullYear(); }))
        svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ||d.getFullYear() == 2020){return 1} else {return 0}})})
        svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : 'middle' })})
        svg.call(function(g){g.selectAll('line').remove()})
        svg.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y0 = Math.max(Math.abs(d3.min(data, function(d) {return d.pib_data})), Math.abs(d3.max(data, function(d) {return d.pib_data})));

    let y = d3.scaleLinear()
        .domain([-y0,2])
        .range([height,0])
        .nice();

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y).ticks(6))
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.select('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", function(d) {return d == 0 ? '#d9d9d9' : '#343434'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById('' + lineaEjeX + '').getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .attr("transform", 'translate(0,0)')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.pib_data < 0 ? '#fde5e1' : '#ffbec3'})
        .attr('y', function(d) {return y(Math.max(0,d.pib_data)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.pib_data) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipProduccion.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipProduccion.html('<span style="font-weight: 300;">' + d.trimestre + '</span><br><span style="font-weight: 900;">' + d.pib_data.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - 25) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipProduccion.style('display','none').style('opacity','0');
        })
}

function getDemandaHistorica(data){
    let contenedorGrafico = document.getElementById('demanda-consumo-minorista');

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart5-1').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart5-1').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'DEMANDA';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart5-1').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart5-1').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart')[0];
    let lineaEje = 'empresasActivasEjeY';

    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 5, bottom: 20, left: 35},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleTime()
        .domain(d3.extent(data, function(d) {return formatTime(d.mes_v2, "mes en letras")}))
        .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x))
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 2000 || d.getFullYear() == 2006 || d.getFullYear() == 2014 || d.getFullYear() == 2020){return 1} else {return 0}})})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 2000 ? 'start' : 'middle' })})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        //.domain([0,d3.max(data, (d) => {return d.indice_precios_corrientes})])
        .domain([0,125])
        .range([height,0]);

    let yAxis = function(g){
        g.attr("transform", 'translate(0,0)')
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .x(function(d){return x(formatTime(d.mes_v2, "mes en letras"))})
        .y(function(d){return y(d.indice_precios_corrientes)});
    
    chart.append("path")
        .datum(data.filter(line.defined()))
        .attr("fill", "none")
        .attr("stroke", "#59fba5")
        .attr("stroke-width", 1)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Dibujo círculos
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(formatTime(d.mes_v2, "mes en letras")); })
        .attr("cy", function(d) { return y(d.indice_precios_corrientes); })
        .style("fill", '#000')
        .style('opacity', '0')
        .attr('transform', 'translate(2.5, 0)')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipDemanda.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDemanda.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.indice_precios_corrientes.toFixed(2).replace('.',',') + '</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipDemanda.style('display','none').style('opacity','0');
        });
}

function getExpectativasConfianzaConsumidor(data){
    let contenedorGrafico = document.getElementById('expectativas-confianza-empresas')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart2-4').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart2-4').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'EXPECTATIVAS';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart2-4').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart2-4').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart')[0];

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend-three');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--expectativas-primero');
    primerPunto.textContent = 'Manufacturas';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--expectativas-tercero');
    segundoPunto.textContent = 'Servicios';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--expectativas-cuarto');
    tercerPunto.textContent = 'Total';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);

    grafico.appendChild(divLeyenda);

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 2.5, bottom: 20, left: 35},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom - (window.innerWidth < 425 ? 50 : 25);

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    let x = d3.scaleBand()
        .domain(data.map(function(d) { return d.mes_v2 }))
        .range([0, width])
        .padding(0.1);
    
    let y = d3.scaleLinear()
        .range([height,0])
        .domain([0,60]);

    //Eje X
    let xAxis = function(g){
        g.call(d3.axisBottom(x).tickFormat(function(d) {return d.substr(-4)}))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d,i) {return i == 0 || i == data.length - 1 ? '1' : '0'}).attr('text-anchor',function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle'})});
    }
        
    chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    //Eje Y
    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(5))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
    }        
    
    chart.append("g")
        .call(yAxis);

    // define the 1st line
    var valueline = d3.line()
        .x(function(d) { return x((d.mes_v2)); })
        .y(function(d) { return y(d.manufacturas); });

    // define the 2nd line
    var valueline2 = d3.line()
        .x(function(d) { return x(d.mes_v2); })
        .y(function(d) { return y(d.servicios); });

    var valueline3 = d3.line()
        .x(function(d) { return x(d.mes_v2); })
        .y(function(d) { return y(d.total_economia); });

    // Add the valueline path.
    chart.append("path")
        .data([data])
        .attr("fill", "none")
        .attr("stroke","#661e1e")
        .attr("stroke-width","1px")
        .attr("d", valueline);

    // Add the valueline2 path.
    chart.append("path")
        .data([data])
        .attr("fill", "none")
        .attr("stroke","#ff4a4a")
        .attr("stroke-width","1px")
        .attr("d", valueline2);

    // Add the valueline2 path.
    chart.append("path")
        .data([data])
        .attr("fill", "none")
        .attr("stroke","#ffc8c8")
        .attr("stroke-width","1px")
        .attr("d", valueline3);

    //Añadir círculos para las tres líneas
    //Manufacturas
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.manufacturas); })
        .style("fill", '#000')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('background-color', '#661e1e')
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.manufacturas.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0').style('background-color', '#ff4a4a');
        })

    //Servicios
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.servicios); })
        .style("fill", '#000')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('background-color', '#ff4a4a')
                .style('display', 'block')
                .style('opacity','0.9')
                tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.servicios.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0').style('background-color', '#ff4a4a');
        })

    //Total economía
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.mes_v2); })
        .attr("cy", function(d) { return y(d.total_economia); })
        .style("fill", '#000')
        .style('opacity', '0')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('background-color', '#ffc8c8')
                .style('display', 'block')
                .style('opacity','0.9')
                tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.total_economia.toFixed(1).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0').style('background-color', '#ff4a4a');
        })    
}

function getExpectativasPrevisionParo(data){
    let contenedorGrafico = document.getElementById('expectativas-prevision-hogares')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart2-2').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart2-2').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'EXPECTATIVAS';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart2-2').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart2-2').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart')[0];

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 5, bottom: 20, left: 30},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleBand()
        .range([0,width])
        .domain(data.map(function(d) { return d.mes }))

    let xAxis = function(g){
        g.call(d3.axisBottom(x2).tickFormat(function(d,i) {if(i == 0){return '2007'} else if(i == data.length -1){ return '2020'}}))
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle' })})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y = d3.scaleLinear()
        .domain([d3.min(data, function(d) {return d.situacion_empleo}),d3.max(data, function(d) {return d.situacion_empleo})])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()});
    }        

    chart.append('g')
        .call(yAxis);

    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.situacion_empleo < 0 ? '#ff4a4a' : '#ffc8c8'})
        .attr('y', function(d) { return y(Math.max(0,d.situacion_empleo)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.situacion_empleo) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><br><span style="font-weight: 900;">' + d.situacion_empleo.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d){
            tooltipExpectativas.style('display','none').style('opacity','0');
        })
}

function getEmpleoAfiliacionHistorico(data){    
    let contenedorGrafico = document.getElementById('empleo-afiliacion-diaria')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart4-2').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    // let primerSubtitulo = document.createElement('span');
    // primerSubtitulo.setAttribute('class', 'first-subtitle')
    // primerSubtitulo.textContent = commonElementsMap.get('#chart4-2').subtitulo_interior;
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'EMPLEO';

    //subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart4-2').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart4-2').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart')[0];
    let lineaEjeX = 'empleo-afiliacion-historico-ejeX';
    
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 5, bottom: 20, left: 55},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleTime()
        .domain(d3.extent(data, function(d) {return formatDayTime(d.fecha)}))
        .range([0, width]);
    
    let xAxis = function(g){
        g.call(d3.axisBottom(x).tickValues(x.domain().filter(function(d,i) { return !(i%1) })).tickFormat(function(d){ return meses[d.getMonth()];}))
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','1')})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : 'end'})});
    }

    chart.append('g')        
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

    //Eje Y
    let y = d3.scaleLinear()
        //.domain(d3.extent(data, function(d) {return d.afiliados}))
        .domain([d3.min(data, function(d){ return d.afiliados - 100000 }),19500000])
        .range([height,0]);

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(4).tickFormat(d3.format(".3s")))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr('x2','0')
            .attr('x1','' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')
            .style('stroke','#343434')
            .style('stroke-width','1')
            .style('opacity', function(d) {return d == 0 ? '1' : '0'})});
    }
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3.line()
        .x(function(d){ return x(formatDayTime(d.fecha))})
        .y(function(d){return y(d.afiliados)});
    
    chart.append("path")
        .datum(data)
        .attr("fill", "none")
        .attr("stroke", "#ffe68c")
        .attr("stroke-width", 0.8)
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Area > set the gradient
    chart.append("linearGradient")				
    .attr("id", 'empleo-afiliacion')			
    .attr("gradientUnits", "userSpaceOnUse")	
    .attr("x1", 0).attr("y1", y(d3.min(data, function(d) {return d.afiliados})))			
    .attr("x2", 0).attr("y2", y(d3.max(data, function(d) {return d.afiliados})))		
    .selectAll("stop")						
    .data([
        {offset: "0%", color: "rgba(255, 230, 140, 0)"},
        {offset: "70%", color: "rgba(255, 230, 140, 0.2)"},
        {offset: "100%", color: "rgba(255, 230, 140, 0.45)"}	
    ])					
    .enter()
    .append("stop")			
    .attr("offset", function(d) { return d.offset; })	
    .attr("stop-color", function(d) { return d.color; });

    let area = d3.area()
        .x(function(d) { return x(formatDayTime(d.fecha)) })	
        .y0(height)					
        .y1(function(d) { return y(d.afiliados); });

    chart.append('path')
        .datum(data)
        .attr('fill', 'url(#empleo-afiliacion)')
        .attr('d', area);

    //Dibujo círculos
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(formatDayTime(d.fecha)) })
        .attr("cy", function(d) { return y(d.afiliados); })
        .style("fill", '#000')
        .style('opacity', '0')
        .attr('transform', 'translate(2.5, 0)')
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipEmpleo.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipEmpleo.html('<span style="font-weight: 300;">' + formatTooltipDate(d.fecha) + '</span><br><span style="font-weight: 900;">' + getNumberWithCommas(d.afiliados) + '</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipEmpleo.style('display','none').style('opacity','0');
        })
}

function getProduccionEmpresasActivas(data){
    let contenedorGrafico = document.getElementById('produccion-numero-empresas')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart1-4').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart1-4').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'PRODUCCIÓN';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart__empresas-activas');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart1-4').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart1-4').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart__empresas-activas')[0];
    let lineaEje = 'empresasActivasEjeY';

    function comparativa(a,b){
        const datoA = a.cambio_data;
        const datoB = b.cambio_data;

        let comparacion = 0;
        if (datoA > datoB) {
            comparacion = -1;
        } else if (datoA <= datoB) {
            comparacion = 1;
        }
        return comparacion;
    }

    data = data.sort(comparativa);

    //Estructura fundamental del gráfico
    let margin = {top: 25, right: 25, bottom: 25, left: 125},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Y axis
    let y = d3.scaleBand()
    .range([0, height])
    .domain(data.map(function(d) { return d.provincia; }))
    .paddingInner(.8)
    .align(1);

    let yAxis = function(svg){
        svg.call(d3.axisLeft(y))
        svg.call(function(g){g.selectAll('.domain').attr('id', lineaEje).style('opacity','0')})
        svg.call(function(g){g.selectAll('.tick text').style('font-size','15px').style('opacity','1').style('font-weight','300')})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append("g")
        .call(yAxis);

    //X axis
    let x = d3.scaleLinear()
        .domain([d3.min(data, function(d) { return d.cambio_data - 1}), data.length > 10 ? d3.max(data, function(d) { return d.cambio_data}) : 0])
        .range([0, width]);

    let xAxis = function(svg){
        svg.call(d3.axisBottom(x).ticks(5))
        svg.call(function(g){g.selectAll('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        svg.call(function(g){g.selectAll('.tick line')
            .attr('y1','0')
            .attr('y2','-' + (document.getElementById(lineaEje).getBoundingClientRect().height) +'')
            .style('stroke','#343434')
            .style('stroke-dasharray','2')})
        svg.attr("transform", "translate(0," + (height + 2.5) + ")");
    }
    
    let xAxis2 = function(svg){
        svg.call(d3.axisTop(x).ticks(5))
        svg.call(function(g){g.selectAll('.domain').remove()})
        svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
    }  

    chart.append("g")
        .call(xAxis);

    chart.append("g")
        .call(xAxis2);

    chart.selectAll('.bar-chart')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', function(d) {return d.cambio_data < 0 ? '#ffbec3' : '#fde5e1'})
        .attr('x', function(d) {return x(Math.min(0, d.cambio_data))})
        .attr('y', function(d) { return y(d.provincia) })
        .attr('width', function(d) { return Math.abs(x(d.cambio_data) - x(0))})
        .attr('height', y.bandwidth())
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipProduccion.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipProduccion.html('<span style="font-weight: 900;">' + d.cambio_data.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px"); 
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipProduccion.style('display','none').style('opacity','0');
        });
}

function getCuentasDeudaPublica(data){
    let contenedorGrafico = document.getElementById('ccpp-deuda-espana')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart3-2').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart3-2').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'CUENTAS PÚBLICAS';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart3-2').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart3-2').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart')[0];
    let lineaEjeX = 'cuentas-deuda-ejeX';

    //Estructura fundamental del gráfico
    let margin = {top: 0, right: 5, bottom: 20, left: 35},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom;
    
    //Inicialización del gráfico
    let chart = d3.select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleBand()
        .range([0,width])
        .domain(d3.range(data.length));

    let x2 = d3.scaleTime()
        .range([0,width])
        .domain(d3.extent(data, function(d) { return formatTime(d.trimestre, 'cuentas_publicas') }));

    let xAxis = function(g){
        g.call(d3.axisBottom(x2))
        g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')})
        g.call(function(g){g.selectAll('.tick line').remove()})
        g.call(function(g){g.selectAll('.tick').attr('opacity', function(d){return d.getFullYear() == 2020 || d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ? '1' : '0'})})
        g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d){return d.getFullYear() == 2020 ? 'end' : 'middle'})});            
    }     

    chart.append('g')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);
    
    //Eje Y
    let y0 = Math.max(Math.abs(d3.min(data, function(d) {return d.porcentaje_deuda})), Math.abs(d3.max(data, function(d) {return d.porcentaje_deuda})));

    let y = d3.scaleLinear()
        .domain([0,y0])
        .range([height,0])
        .nice();

    let yAxis = function(g){
        g.call(d3.axisLeft(y).ticks(6))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }        

    chart.append('g')
        .call(yAxis);

    //Visualización
    chart.selectAll('.barras')
        .data(data)
        .enter()
        .append('rect')
        .attr('fill', '#ff9574')
        .attr('y', function(d) {return y(Math.max(0,d.porcentaje_deuda)) })
        .attr('x', function(d,i) { return x(i)})
        .attr('height', function(d) {return Math.abs(y(d.porcentaje_deuda) - y(0));})
        .attr('width', x.bandwidth() / 2)
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
    
            tooltipCuentas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipCuentas.html('<span style="font-weight: 300;">' + formatTooltipDate(d.trimestre) + '</span><br><span style="font-weight: 900;">' + d.porcentaje_deuda.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipCuentas.style('display','none').style('opacity','0');
        })
}

function getCuentasRecaudacion(data){
    let contenedorGrafico = document.getElementById('ccpp-variacion-impuestos')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart3-3').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart3-3').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'CUENTAS PÚBLICAS';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart3-3').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart3-3').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart')[0];
    let lineaEjeY = 'cuentasRecaudacionY';

    // //Crear botones para mapa o gráfico de barras
    let grupoBotones = document.createElement('div');
    grupoBotones.classList.add('chart-buttons');

    let firstButton = document.createElement('button');
    firstButton.textContent = 'Porcentaje';
    firstButton.classList.add('button');
    firstButton.classList.add('button__cuentas');
    firstButton.classList.add('button-active');
    let secondButton = document.createElement('button');
    secondButton.textContent = 'Millones';
    secondButton.classList.add('button');
    secondButton.classList.add('button__cuentas');
    let thirdButton = document.createElement('button');
    thirdButton.textContent = 'Acumulada';
    thirdButton.classList.add('button');
    thirdButton.classList.add('button__cuentas');

    grupoBotones.appendChild(firstButton);
    grupoBotones.appendChild(secondButton);
    grupoBotones.appendChild(thirdButton);
    grafico.insertBefore(grupoBotones, grafico.firstChild);

    //Asociamos los botones a un evento que cambiará la forma de datos
    let tipo = 'porcentaje';

    firstButton.addEventListener('click', function() {
        updateChart('porcentaje')
        firstButton.classList.add('button-active');
        secondButton.classList.remove('button-active');
        thirdButton.classList.remove('button-active');
        getOutTooltips();
    });
    secondButton.addEventListener('click', function() {
        updateChart('millones');
        secondButton.classList.add('button-active');
        firstButton.classList.remove('button-active');
        thirdButton.classList.remove('button-active');
        getOutTooltips();
    });
    thirdButton.addEventListener('click', function() {
        updateChart('acumulada');
        secondButton.classList.remove('button-active');
        firstButton.classList.remove('button-active');
        thirdButton.classList.add('button-active');
        getOutTooltips();
    });

    //Estructura fundamental del gráfico
    let alturaGrafico = 57.5;

    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 10, bottom: 20, left: 145},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom - alturaGrafico;

    // append the svg object to the body of the page
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje Y
    let y = d3.scaleBand()
        .range([0, height])
        .domain(data.map(function(d) { return d.figura_mostrar; }));

    let yAxis = function(g){
        g.call(d3.axisLeft(y))
        g.call(function(g){g.selectAll('.domain').attr('id', lineaEjeY).style('opacity','0')})
        g.call(function(g){g.selectAll('.tick text').style('font-size', '15px').style('opacity','1').style('font-weight','300')})
        g.call(function(g){g.selectAll('.tick line').remove()});
    }        

    chart.append("g")
        .attr('class', 'y-axis')
        .call(yAxis);
    
    //Eje X
    let x = d3.scaleLinear()
        .range([20, width]);

    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(3))
        g.attr("transform", "translate(0," + (height) + ")")
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr('y1','0')
            .attr('y2', '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')
            .style('stroke','#343434')
            .style('stroke-dasharray','.8')})
    }

    function createChart(){
        //X axis
        x.domain([-100, 50]);

        chart.append("g")
            .attr('class', 'x-axis')
            .call(xAxis);

        // Lines
        chart.selectAll("myline")
        .data(data)
        .enter()
        .append("line")
            .attr('class','barras')
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) { return x(d.variacion_porc); })
            .attr("y1", function(d) { return y(d.figura_mostrar) + (y.bandwidth() / 2); })
            .attr("y2", function(d) { return y(d.figura_mostrar) + (y.bandwidth() / 2); })
            .attr("stroke", "#ff9574")
            .attr("stroke-width", '4px')
            .on('touchstart mouseover', function(d) {
                //Dato a mostrar
                if(tipo == 'porcentaje'){
                    dato = d.variacion_porc.toFixed(2).replace('.',',') + '%';
                } else if (tipo == 'acumulada'){
                    dato = d.variacion_acumulada_porc.toFixed(2).replace('.',',') + '%';
                } else {
                    dato = d.variacion_millones;
                }

                //Disposición del tooltip
                let bodyWidth = parseInt(d3.select('body').style('width'));
                let mapHeight = parseInt(d3.select('body').style('height'));
        
                // let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
                // let downSide = mapHeight / 2 > d3.event.pageY ? false : true;
        
                tooltipCuentas.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipCuentas.html('<span style="font-weight: 900;">' + dato + '</span>')
                    .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                    .style("top", ""+ (d3.event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipCuentas.style('display','none').style('opacity','0');
            })
    }

    function updateChart(type){        
        if(type == 'porcentaje'){
            x.domain([-100,50])
        } else if ( type == 'acumulada'){
            x.domain([-20,10])
        } else {
            x.domain(d3.extent(data, function(d) { return d.variacion_millones }))
        }
        tipo = type;
        
        let chart = d3.select(grafico).transition();

        chart.select('.x-axis')
            .duration(750)
            .call(xAxis);

        chart.selectAll('.barras')
            .duration(750)
            .attr("x1", function(d) { return x(0); })
            .attr("x2", function(d) {
                if(type == 'porcentaje'){
                    return x(d.variacion_porc); 
                } else if (type == 'acumulada'){
                    return !isNaN(d.variacion_acumulada_porc) ? x(d.variacion_acumulada_porc) : x(0);                 
                } else {
                    return x(d.variacion_millones); 
                }                
            });
    }

    //Por defecto, se crea con porcentaje
    createChart();
}

function getExpectativasPrevisiones(data){
    let contenedorGrafico = document.getElementById('expectativas-prevision-crecimiento')

    /* Pintado dinámico de elementos */
    let titular = document.createElement('h2');
    titular.setAttribute('class','chart-title');
    titular.textContent = commonElementsMap.get('#chart2-3').titulo_interior;
    contenedorGrafico.appendChild(titular);

    let subtitulo = document.createElement('div');
    subtitulo.setAttribute('class','chart-subtitle');

    let primerSubtitulo = document.createElement('span');
    primerSubtitulo.setAttribute('class', 'first-subtitle')
    primerSubtitulo.textContent = commonElementsMap.get('#chart2-3').subtitulo_interior + " |";
    
    let segundoSubtitulo = document.createElement('span');
    segundoSubtitulo.setAttribute('class','second-subtitle');
    segundoSubtitulo.textContent = 'EXPECTATIVAS';

    subtitulo.appendChild(primerSubtitulo);
    subtitulo.appendChild(segundoSubtitulo);
    
    contenedorGrafico.appendChild(subtitulo);

    let apartadoGrafico = document.createElement('div');
    apartadoGrafico.setAttribute('class','chart__previsiones-crecimiento');
    contenedorGrafico.appendChild(apartadoGrafico);

    let contenedorInferior = document.createElement('div');
    contenedorInferior.setAttribute('class','chart-note');

    let fuenteDatos = document.createElement('strong');
    fuenteDatos.setAttribute('class','chart-source');
    fuenteDatos.textContent = 'Fuente: ' + commonElementsMap.get('#chart2-3').fuente_interior;

    let actualizacion = document.createElement('span');
    actualizacion.setAttribute('class','chart-date');
    let fecha = commonElementsMap.get('#chart2-3').ultima_actualizacion.split("/");        
    actualizacion.textContent = 'Datos actualizados a ' + fecha[0] + ' de ' + meses[+fecha[1] - 1].toLocaleLowerCase() + ' de ' + fecha[2];

    contenedorInferior.appendChild(fuenteDatos);
    contenedorInferior.appendChild(actualizacion);

    contenedorGrafico.appendChild(contenedorInferior);

    /* Desarrollo del gráfico */
    let grafico = contenedorGrafico.getElementsByClassName('chart__previsiones-crecimiento')[0];
    let lineaEjeY = 'expectativas-previsiones-ejeY';

    //Creación de leyenda        
    let divLeyenda = document.createElement('div');
    divLeyenda.setAttribute('class','legend-three');

    let primerPunto = document.createElement('p');
    primerPunto.setAttribute('class','legend__item legend__item--expectativas-primero');
    primerPunto.textContent = 'III trim.';

    let segundoPunto = document.createElement('p');
    segundoPunto.setAttribute('class','legend__item legend__item--expectativas-segundo');
    segundoPunto.textContent = 'IV trim.';

    let tercerPunto = document.createElement('p');
    tercerPunto.setAttribute('class','legend__item legend__item--expectativas-tercero');
    tercerPunto.textContent = 'I trim. 2021';

    divLeyenda.appendChild(primerPunto);
    divLeyenda.appendChild(segundoPunto);
    divLeyenda.appendChild(tercerPunto);

    grafico.appendChild(divLeyenda);
    
    //Estructura fundamental del gráfico
    let margin = {top: 20, right: 77.5, bottom: 20, left: 15},
    width = grafico.clientWidth - margin.left - margin.right,
    height = grafico.clientHeight - margin.top - margin.bottom - (window.innerWidth < 425 ? 50 : 25);
    
    //Modificación del formato de datos
    let newData = [];
    for(let i = 0; i < data.length; i++){        
        newData.push({categoria: data[i]['pais'], valores: [{descriptor:'I trim. 2021', valor: data[i].trimestre_3},  
            {descriptor:'IV trim.', valor: data[i].trimestre_2}, {descriptor:'III trim.', valor: data[i].trimestre_1} ]});            
    }

    let paises = newData.map(function(d) { return d.categoria });
    let descriptores = newData[0].valores.map(function(d) { return d.descriptor});

    //Estructura del gráfico
    let chart = d3.select(grafico)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje Y
    let y0 = d3.scaleBand()
        .rangeRound([height,0])
        .domain(paises)
        .padding(.5)
        .paddingOuter(.1);

    let y1 = d3.scaleBand()
        .range([y0.bandwidth(),0])
        .domain(descriptores);

    let yAxis = function(g){
        g.call(d3.axisRight(y0).tickSize(0))
        g.call(function(g){g.selectAll('.tick text')
            .style('fill', '#fff')
            .style('font-size', '14px')
            .style('font-family','Roboto')
            .style('font-weight','300')
            .style('opacity','1')})
        g.call(function(g){g.select('.domain').attr('id', lineaEjeY).style('opacity','0')});
    }        

    chart.append("g")        
        .attr('transform', 'translate(' + (width + 10) + ',0)')
        .call(yAxis);
    
    //Eje X
    let x = d3.scaleLinear()
        .range([width,0])
        .domain([0.5,-12.5]);

    let xAxis = function(g){
        g.call(d3.axisBottom(x).ticks(5))
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.selectAll('.tick line')
            .attr("stroke-opacity", 0.5)
            .attr("stroke", "#343434")
            .attr("stroke-width", "1")
            .attr("y1", '0%')
            .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')});
    }        

    chart.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);
    
    //Visualización de datos
    let slice = chart.selectAll(".slice")
      .data(newData)
      .enter().append("g")
      .attr("class", "g")
      .attr("transform",function(d) { return "translate(0," + y0(d.categoria) + ")"; });

    slice.selectAll("rect")
        .data(function(d) { return d.valores; })
        .enter()
        .append("rect")
        //.attr('width', function(d) { return x(0) - x(d.valor)})
        .attr("width", function(d) { return Math.abs(x(d.valor) - x(0)); })
        .attr('height', y1.bandwidth())
        .style('fill',  function(d){return d.descriptor == 'III trim.' ? '#661e1e' : d.descriptor == 'IV trim.' ? '#992d2d' : '#ff4a4a'})
        //.attr('x', function(d) { return x(d.valor)})
        .attr('x', function(d) { return d.valor > 0 ? x(0) : x(d.valor)})
        .attr('y', function(d) { return y1(d.descriptor)})
        .on('touchstart mouseover', function(d) {
            let bodyWidth = parseInt(d3.select('body').style('width'));
            let mapHeight = parseInt(d3.select('body').style('height'));
    
            tooltipExpectativas.transition()     
                .duration(200)
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipExpectativas.html('<span style="font-weight: 900;">' + d.valor.toFixed(2).replace('.',',') + '%</span>')
                .style("left", ""+ (d3.event.pageX - (window.innerWidth < 525 ? 40 : 25)) +"px")     
                .style("top", ""+ (d3.event.pageY - 35) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipExpectativas.style('display','none').style('opacity','0');
        })
}

/*
*
* Helpers de visualizacion
*
*/

function isResponsive() {
    return window.innerWidth < 993;
}

function isMobile() {
    return window.innerWidth < 768;
}

function formatTimeData(data){
    let result = [];
    for(let i = 0; i < data.length; i++){
        let item = data[i];
        let provincia = '';
        if (!Object.entries) {
            Object.entries = function( obj ){
                var ownProps = Object.keys( obj ),
                    i = ownProps.length,
                    resArray = new Array(i); // preallocate the Array
                while (i--)
                    resArray[i] = [ownProps[i], obj[ownProps[i]]];
            
                return resArray;
            };
        } else {
            Object.entries(item).forEach(function(item2, index) {
                if(index == 0){
                    provincia = item2[1];
                } else {
                    result.push({fecha: item2[0], provincia: provincia, dato: item2[1]});
                }            
            });
        }        
    }
    return result;
}

function scaleBandPosition(mouse, x) {
    var xPos = mouse[0];
    var domain = x.domain(); 
    var range = x.range();
    var rangePoints = d3.range(range[0], range[1], x.step());
    let prueba = d3.bisectLeft(rangePoints, xPos);
    return domain[prueba];
}

function formatTime(date) {
    let split = date.split("/");
    return new Date(split[1], split[0] - 1, 1);
}

function formatTime(date, especifico) {
    let split = date.split("/");
    let number = 0;
    let year = split[1];
    if(especifico == 'cuentas_publicas'){
        for(let i = 0; i < mesesReducido.length; i++){
            if(split[0].substr(0,3).toLowerCase() == mesesReducido[i].toLowerCase()){
                number = i;
            }
        }
        year = year < 25 ? '20' + year : '19' + year; //Feo > Peor hay que adecuarse a lo dispuesto por Javier
    } else {
        for(let i = 0; i < meses.length; i++){
            if(split[0].toLowerCase() == meses[i].toLowerCase()){
                number = i;
            }
        }
    }
    
    return new Date(year, number, 1);
}

function formatMonthTime(date){
    let split = date.split("/");
    return new Date(split[1], 4 - 1, 1);
}

function formatDayTime(date){
    let split = date.split("/");
    return new Date(split[2], split[1] - 1, split[0]);
}

function formatYear(date) {
    let prueba = date.getFullYear();
    return prueba;
}

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function formatTooltipDate(date){
    //Puede venir 'febrero/2007' o '7/2007' o '7/07/2020'
    let split = date.split("/");
    let dia = '', mes = '', anio = '';

    let resultado = '';

    if(isNaN(split[0])){
        mes = split[0];
        let letra = mes.charAt(0);
        letra = letra.toUpperCase();
        mes = letra + mes.slice(1);
        anio = split[1];

        resultado = mes + ' ' + anio;

    } else {
        let indice;
        if(split.length > 2){
            indice = parseInt(split[1]) - 1;
            anio = split[2];
            mes = meses[indice].toLowerCase();
            dia = split[0];

            resultado = dia + ' de ' + mes + ' ' + anio;
        } else {
            indice = parseInt(split[0]) - 1;
            anio = split[1];
            mes = meses[indice];

            resultado = mes + ' ' + anio;
        }      
        
    }
    return resultado;
}

//Línea de regresión
function calculateRegression(XaxisData, Yaxisdata) {
    let ReduceAddition = function(prev, cur) { return prev + cur; };
    
    // finding the mean of Xaxis and Yaxis data
    let xBar = XaxisData.reduce(ReduceAddition) * 1.0 / XaxisData.length;
    let yBar = Yaxisdata.reduce(ReduceAddition) * 1.0 / Yaxisdata.length;

    let SquareXX = XaxisData.map(function(d) { return Math.pow(d - xBar, 2); })
        .reduce(ReduceAddition);
    
    let ssYY = Yaxisdata.map(function(d) { return Math.pow(d - yBar, 2); })
        .reduce(ReduceAddition);
        
    let MeanDiffXY = XaxisData.map(function(d, i) { return (d - xBar) * (Yaxisdata[i] - yBar); })
        .reduce(ReduceAddition);
        
    let slope = MeanDiffXY / SquareXX;
    let intercept = yBar - (xBar * slope);
    
    // returning regression function
    return function(x){
        return x*slope+intercept;
    }
}

//Invisibilidad de tooltips
function getOutTooltips(){
    let tooltips = document.getElementsByClassName('tooltip-dashboard');
    for(let i = 0; i < tooltips.length; i++){
        tooltips[i].style.display = 'none';
        tooltips[i].style.opacity = '0';
    }
}

function leaveCharts(){
    let charts = document.getElementsByClassName('chart-content');
    for(let i = 0; i < charts.length; i++){
        charts[i].addEventListener('mouseleave', function(){
            getOutTooltips();
        })
    }
    let chart = document.getElementsByClassName('header-chart')[0];
    chart.addEventListener('mouseleave', function() {
        console.log("entra");
        getOutTooltips();
    })
}

/*
*
* Helpers de datos
*
*/

/* Función AJAX para carga de datos */
function getData(data, type) {
    let http = new XMLHttpRequest();
    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let response = http.response;
            if(response == ""){
                console.error('No hay nada. Error en la llamada');
            } else {
                let formatData = type == 'commonElements' ? csvToJson(response, 'elementos-comunes') : csvToJson(response);
                initData(formatData, type);
            }   
        }
    }
    http.open('get', data, false);
    http.send();
}

/* Función para parsear datos de TSV a objetos JavaScript --> Algo tendremos que hacer para titulares, subtítulos y fuentes */
function csvToJson(csv, dataHome){
    let lines=csv.split("\n");    
    let result = [];

    let headers = dataHome ? lines[1].split("\t") : lines[3].split("\t");
    let inicio = dataHome ? 2 : 4;

    for(i = 0; i < headers.length; i++){
        headers[i] = headers[i].trim();
    }

    for(let i=inicio;i<lines.length;i++){
        let obj = {};
        let currentline=lines[i].split("\t");

        for(let j=0;j<headers.length;j++){
            if(headers[j] != ''){
                obj[headers[j]] = currentline[j];
            }            
        }
        result.push(obj);
    }
    return result;
}

/* Recogemos los datos para integrarlos en los diferentes indicadores >> Borrar los que no sean necesarios */
function initData(data, typeOfData){    
    if (typeOfData == 'dataProduccion') {
        for(let i = 0; i < data.length; i++){
            data[i].PIB != '' && produccionPib.push({trimestre: data[i].Trimestre_PIB, trimestre_v2: data[i].Trimestre_PIB_Generico, pib_data: +data[i].PIB.replace(',','.')});            
            data[i].ID_PROVINCIA != '' && produccionEmpresasActivas.push({id_provincia: +data[i].ID_PROVINCIA, provincia: data[i].Provincia, cambio_data: +data[i].Cambio_desde_inicio_crisis.replace(',','.')});
        }
    } else if (typeOfData == 'dataEmpleo') {
        for(let i = 0; i < data.length; i++){
            data[i].Periodo_Afiliacion != '' && empleoAfiliacionHistorico.push({fecha: data[i].Periodo_Afiliacion, afiliados: +data[i].Afiliados});
        }
    } else if (typeOfData == 'dataDemanda') {
        for(let i = 0; i < data.length; i++){
            data[i].Mes_Historico != '' && demandaMinoristaHistorica.push({mes: data[i].Mes_Historico, mes_v2: data[i].Mes_Historico_Generico, indice_precios_corrientes: +data[i]['Índice'].replace(',','.')});
        }
    } else if (typeOfData == 'dataExpectativas') {
        for(let i = 0; i < data.length; i++){
            data[i].ID_PAIS != '' && expectativasPrevisiones.push({id_pais: data[i].ID_PAIS, pais: data[i].PAIS, trimestre_1: +data[i]['III Trimestre'].replace(',','.'), trimestre_2: +data[i]['IV Trimestre'].replace(',','.'), trimestre_3: +data[i]['I Trimestre 2021'].replace(',','.')}); //Cambiar
            data[i].Mes_Confianza != '' && expectativasConfianzaConsumidor.push({mes: data[i].Mes_Confianza, mes_v2: data[i].Mes_Confianza_Generico, manufacturas: +data[i]['Manufacturas'].replace(',','.'), servicios: +data[i]['Servicios'].replace(',','.'), total_economia: +data[i]['Total economía'].replace(',','.')});
            data[i].Mes_Evolucion_Paro != '' && expectativasHogaresParo.push({mes: data[i]['Mes_Evolucion_Paro'], mes_v2: data[i]['Mes_Evolucion_Paro_Generico'], situacion_empleo: +data[i]['Unemployment expectations over the next 12 months'].replace(',','.')});
        }
    } else if (typeOfData == 'dataSectores') {
        for(let i = 0; i < data.length; i++){
        }
    } else if (typeOfData == 'dataCuentasPublicas') {
        for(let i = 0; i < data.length; i++){
            data[i]['Figura tributaria'] != '' && cuentasRecaudacion.push({figura_tributaria: data[i]['Figura tributaria'], figura_mostrar: data[i].Figura_Mostrar, variacion_porc: +data[i]['Variación en porcentaje'].replace(',','.'), variacion_millones: +data[i]['Variación en millones de euros'].replace(',','.'), variacion_acumulada_porc: +data[i]['Variación acumulada en lo que va de año en porcentaje'].replace(',','.')});
            data[i].Trimestre_Deuda_Generico != '' && cuentasDeudaPublica.push({trimestre: data[i].Trimestre_Deuda_Generico, porcentaje_deuda: +data[i].Porcentaje_Deuda.replace(',','.')});
        }
    } else if (typeOfData == 'commonElements'){
        for(let i = 0; i < data.length; i++){
            commonElementsMap.set(data[i].ID_Grafico, {id_grafico: data[i].ID_Grafico, area: data[i].Area_Interes, area_desarrollo: data[i].Area_Interes_Desarrollo, titulo_area_home: data[i].Titulo_Portada_Area, titulo_interior: data[i].Titulo, subtitulo_interior: data[i].Subtitulo, fuente_interior: data[i].Fuente, ultima_actualizacion: data[i].Ultima_Actualizacion, descriptor_ejeX: data[i].Descriptor_EjeX_Opcional, descriptor_ejeY: data[i].Descriptor_EjeY_Opcional});
        }
    }    
}