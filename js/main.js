//Visibilidad de sumarios
let summary = document.querySelectorAll(".summary");
let viewportHeight = window.innerHeight;
//Redes sociales
let sharing = document.getElementById("sharing");
let footer = document.getElementsByTagName('footer')[0];
//Modal
let modal = document.getElementById('modal');
let modalBtn = document.getElementById('modal--close');
//Scroll arrow
let arrow = document.getElementById('scroll-arrow');

modalBtn.addEventListener('click', function() {
    modal.style.display = 'none';
});

arrow.addEventListener('click', function() {
    let contenido = document.getElementsByTagName('section')[0].getBoundingClientRect();
    window.scrollTo({top: contenido.top - 40, behavior: 'smooth'});
});

setRRSSLinks();

/* Eventos asociados */
window.addEventListener('scroll', function(){
    let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);

    //Aside
	if ( scrollTop > viewportHeight ){
        sharing.classList.add("visible");
        modal.classList.add("visible");
	} else {
        sharing.classList.remove("visible");
        modal.classList.remove("visible");
	}

    //Sumarios
	for (var i = 0; i < summary.length; i++) {
		if(isElementInViewport(summary[i])) {
			if(!summary[i].classList.contains('is-visible')) {
				summary[i].classList.add('is-visible')
			}
		}
    }
    
	for (let i = 0; i < summary.length; i++) {
		if(isElementInViewport(summary[i])) {
			if(!summary[i].classList.contains('is-visible')) {
				summary[i].classList.add('is-visible')
			}
		}
	}
});

/* Helpers */
function isElementInViewport(el) {
    let scroll = window.scrollY || window.pageYOffset
    let boundsTop = el.getBoundingClientRect().top + scroll
    
    let viewport = {
        top: scroll,
        bottom: scroll + viewportHeight,
    }
    
    let bounds = {
        top: Math.floor(boundsTop) + el.offsetHeight,
        bottom: boundsTop + el.clientHeight,
	}
	
    return ( (viewport.bottom > bounds.top) && (viewport.top < bounds.bottom) ) 
}

function setRRSSLinks() {
    var urlPage = window.location.href;

    //Facebook
    var shareFB = document.getElementById("shareFB")
    var fbHref = "https://www.facebook.com/sharer/sharer.php?u="+urlPage
    shareFB.setAttribute("href",fbHref)

    //Twitter
    var shareTW = document.getElementById("shareTW")
    var twText = shareTW.getAttribute("data-text")
    var twHref = "https://twitter.com/intent/tweet?url="+urlPage+"&text="+twText+"&original_referer="+urlPage
    shareTW.setAttribute("href",twHref)

    //Linkedin
    var shareLK = document.getElementById("shareLK")
    var lkText = shareLK.getAttribute("data-text")
    var lkHref = "https://www.linkedin.com/shareArticle?mini=true&url="+urlPage+"&title="+lkText+"&summary=&source="
    shareLK.setAttribute("href",lkHref)

    //WhatsApp
    var shareWA = document.getElementById("shareWA")
    var waText = shareWA.getAttribute("data-text")
    var waHref = "https://api.whatsapp.com/send?text="+waText+" "+urlPage
    shareWA.setAttribute("href",waHref)
}